from flask_unchained import Bundle

class ProjectBundle(Bundle):
    command_group_names = ['project']