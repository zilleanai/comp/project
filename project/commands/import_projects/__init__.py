import click
import json
import os
import sys

from datetime import datetime
from flask.cli import with_appcontext

from flask_unchained.bundles.sqlalchemy import SessionManager
from flask_unchained import unchained, injectable

from backend.config import Config as AppConfig


from ...models import Project

from ..group import project
from .project_data import ProjectData, load_project_datas

PROJECTS_METADATA_PATH = os.path.join(
    AppConfig.APP_CACHE_FOLDER, '.projects-metadata.json')


@project.command()
@click.option('--reset', is_flag=True, default=False, expose_value=True,
              help='Ignore previously updated at timestamps.')
@with_appcontext
def import_projects(reset):
    click.echo('Importing new/updated projects.')
    if _import_projects(reset):
        click.echo('Done.')
    else:
        click.echo('No new projects found. Exiting.')


@unchained.inject('session_manager')
def _import_projects(reset, session_manager: SessionManager = injectable):
    last_updated = load_metadata(reset)
    new_projects = load_project_datas(AppConfig.DATA_FOLDER,
                                      last_updated)
    count = 0
    count += process_project_datas(new_projects, session_manager)

    if count:
        session_manager.commit()
        save_metadata()
    return count


def process_project_datas(project_datas, session_manager: SessionManager):
    count = -1
    for count, project_data in enumerate(project_datas):
        project, is_create = project_data.create_or_update_project()
        session_manager.save(project)
        msg_prefix = ''
        msg_prefix += 'Created' if is_create else 'Updated'
        click.echo(f'{msg_prefix} Project: {project.name}')

    return count + 1


def load_metadata(reset=False):
    if not os.path.exists(AppConfig.DATA_FOLDER):
        click.secho('Could not find directory DATA_FOLDER'
                    f'={AppConfig.DATA_FOLDER}', fg='red')
        sys.exit(1)

    if reset or not os.path.exists(PROJECTS_METADATA_PATH):
        return 0

    with open(PROJECTS_METADATA_PATH) as f:
        metadata = json.load(f)
    return metadata['last_updated']


def save_metadata():
    os.makedirs(os.path.dirname(PROJECTS_METADATA_PATH), exist_ok=True)

    data = json.dumps({'last_updated': datetime.now().timestamp()}, indent=4)
    with open(PROJECTS_METADATA_PATH, 'w') as f:
        f.write(data + '\n')
