import functools
import os
import pytz
import re
import json
from datetime import datetime

from flask_unchained import unchained, injectable

from ...models import Project


class ProjectData(object):
    def __init__(self, dir_entry: os.DirEntry):
        super().__init__()
        self.file_path = dir_entry.path
        self.folder_name = dir_entry.name
        self.is_dir = os.path.isdir(self.file_path)
        self.dir_path = os.path.dirname(self.file_path) \
            if self.is_dir else None
        self.dir_name = self.dir_path.rsplit(os.path.sep, 1)[1] \
            if self.is_dir else None
        print(self.folder_name)

        self.last_updated = datetime.now()

    def create_or_update_project(self):
        is_create = False
        project = Project.get_by(name=self.folder_name)
        if not project:
            project = Project.create(name=self.folder_name)
            is_create = True

        return project, is_create


def load_project_datas(dir_path, last_updated):
    for dir_entry in os.scandir(dir_path):  # type: os.DirEntry
        is_dir = dir_entry.is_dir()
        is_updated = dir_entry.stat().st_mtime > last_updated
        if is_updated and is_dir:
            yield ProjectData(dir_entry)
