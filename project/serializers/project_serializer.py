from flask_unchained.bundles.api import ma

from ..models import Project

PROJECT_FIELDS = ('id', 'created_at', 'updated_at', 'name')


class ProjectSerializer(ma.ModelSerializer):

    class Meta:
        model = Project
        fields = PROJECT_FIELDS


@ma.serializer(many=True)
class ProjectListSerializer(ProjectSerializer):
    class Meta:
        model = Project
        fields = PROJECT_FIELDS
