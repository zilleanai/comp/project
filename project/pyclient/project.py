import requests
from io import BytesIO


class project():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self, path: str = ''):
        url = self.project['api_root'] + 'project/projects'
        return requests.get(url).json()

