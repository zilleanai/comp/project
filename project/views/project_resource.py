import os
from shutil import copy2, copytree, ignore_patterns
import json
from flask import current_app, request
from flask_unchained.bundles.api import ModelResource
from flask_unchained import (
    Controller, injectable, lazy_gettext as _, route, url_for)
from datetime import timezone
from git import Repo
import subprocess

from backend.config import Config as AppConfig

from ..models import Project
from ..services import ProjectManager


class ProjectResource(ModelResource):
    class Meta:
        model = Project
        include_methods = ('create', 'get', 'list')
        include_decorators = ('get', 'list')
        member_param = '<string:name>'

    def __init__(self, project_manager: ProjectManager = injectable):
        super().__init__()
        self.project_manager = project_manager

    def create(self):
        project = request.json['project']
        git_url = request.json.get('git')
        project_path = os.path.join(AppConfig.DATA_FOLDER, project)
        ssh_path = os.path.join(AppConfig.SSH_FOLDER, project)
        subprocess.call(['ssh-keygen', '-b', '2048', '-t', 'rsa', '-f', ssh_path, '-q', '-N', ''])
        if git_url:
            Repo.clone_from(git_url, project_path)
        else:
            skel_path = os.path.join(AppConfig.DATA_FOLDER, 'skel')
            if os.path.exists(skel_path) and not os.path.exists(project_path):
                copytree(skel_path, project_path, ignore=ignore_patterns('.git'))
            r = Repo.init(project_path)
        p = self.project_manager.create(name=project)
        return self.created(p)
