from flask_unchained.bundles.sqlalchemy import ModelManager

from ..models import Project


class ProjectManager(ModelManager):
    class Meta:
        model = Project