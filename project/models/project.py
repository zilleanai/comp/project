from flask_unchained.bundles.sqlalchemy import db


class Project(db.Model):

    name = db.Column(db.String(128))
    __repr_props__ = ('id', 'name')

    @classmethod
    def get_projects(cls):
        return cls.query\
            .all()
