import { createRoutine } from 'actions'
export const changeProject = createRoutine('project/CHANGE_PROJECT')
export const listProjects = createRoutine('project/LIST_PROJECTS')
export const loadProjectData = createRoutine('project/LOAD_PROJECT_DATA')
export const newProject = createRoutine('project/NEW_PROJECT')