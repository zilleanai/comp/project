import { get, post } from 'utils/request'
import { v1 } from 'api'

function project(uri) {
    return v1(`/project${uri}`)
}

export default class Project {
    static listProjects() {
        return get(project('/projects'))
    }
    /**
    * @param {Object} project
    */
    static loadProjectData(id) {
        return get(project(`/projects/${id}`))
    }

    static newProject({ name, git }) {
        return post(project('/projects'), { 'project': name, 'git': git })
    }
}