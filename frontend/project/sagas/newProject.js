import { call, put, takeLatest } from 'redux-saga/effects'

import { newProject } from '../actions'
import { createRoutineFormSaga } from 'sagas'
import ProjectApi from '../api'


export const KEY = 'new_project'

export const newProjectSaga = createRoutineFormSaga(
  newProject,
  function* successGenerator(payload) {
    const response = yield call(ProjectApi.newProject, payload)
    yield put(newProject.success(response))
  }
)

export default () => [
  takeLatest(newProject.TRIGGER, newProjectSaga)
]
