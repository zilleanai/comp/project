import { call, put, takeLatest } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import { flashSuccess } from 'site/actions'
import { ROUTES, ROUTE_MAP } from 'routes'
import { createRoutineSaga } from 'sagas'

import { changeProject } from '../actions'
import { storage } from '..'

export const KEY = 'changeproject'

export const changeProjectSaga = createRoutineSaga(
  changeProject,
  function* successGenerator(payload) {
    storage.setProject(payload.project)
    yield put(changeProject.success())
    yield put(push(ROUTE_MAP[ROUTES.Home].path))
    yield put(flashSuccess(`Project changed to ${payload.project}`))

  },
)

export default () => [
  takeLatest(changeProject.TRIGGER, changeProjectSaga),
]
