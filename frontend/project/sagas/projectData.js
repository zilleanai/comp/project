import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadProjectData } from '../actions'
import ProjectApi from '../api'
import { selectProjectData } from '../reducers/projectData'


export const KEY = 'projectData'

export const maybeLoadProjectDataSaga = function* (project) {
  const { byId, isLoading } = yield select(selectProjectData)
  const isLoaded = !!byId[project.id]
  if (!(isLoaded || isLoading)) {
    yield put(loadProjectData.trigger(project))
  }
}

export const loadProjectDataSaga = createRoutineSaga(
  loadProjectData,
  function* successGenerator({ payload: project }) {
    project = yield call(ProjectApi.loadProjectData, project)
    yield put(loadProjectData.success({ project }))
  }
)

export default () => [
  takeEvery(loadProjectData.MAYBE_TRIGGER, maybeLoadProjectDataSaga),
  takeLatest(loadProjectData.TRIGGER, loadProjectDataSaga),
]
