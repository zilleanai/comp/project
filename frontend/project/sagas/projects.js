import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listProjects } from '../actions'
import ProjectApi from '../api'
import { selectProjects } from '../reducers/projects'


export const KEY = 'projects'

export const maybeListProjectsSaga = function* () {
  const { isLoading, isLoaded } = yield select(selectProjects)
  if (!(isLoaded || isLoading)) {
    yield put(listProjects.trigger())
  }
}

export const listProjectsSaga = createRoutineSaga(
  listProjects,
  function* successGenerator() {
    const projects = yield call(ProjectApi.listProjects)
    yield put(listProjects.success({
      projects: projects.map(convertDates(['createdAt', 'updatedAt'])),
    }))
  },
)

export default () => [
  takeEvery(listProjects.MAYBE_TRIGGER, maybeListProjectsSaga),
  takeLatest(listProjects.TRIGGER, listProjectsSaga),
]
