import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { ProjectList } from '../../components'
import { newProject, listProjects } from '../../actions'
import { TextField } from 'components/Form'
const FORM_NAME = 'newProject'

class Projects extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.projects-page',
          intro: 'On the projects page projects to work with have to be selected. Everything, files, models, ... are connected to a project.',
        },
        {
          element: '.project-name',
          intro: 'Name of a new project.',
        },
        {
          element: '.project-git',
          intro: 'Git url of the project to clone from.',
        },
        {
          element: '.button-primary',
          intro: 'Create a new project here.',
        },
        {
          element: '.project-list',
          intro: 'List of projects to select.',
        },
      ],
    };
  }

  render() {
    const { error, handleSubmit, pristine, submitting } = this.props
    const { stepsEnabled, steps, initialStep } = this.state;
    return (
      <PageContent className='projects-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Projects</title>
        </Helmet>
        <h1>Projects</h1>
        <form onSubmit={handleSubmit(newProject)}>
        <TextField className='project-name' name='name' />
        <TextField className='project-git' name='git' />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Creating...' : 'Create'}
            </button>
          </div>
        </form>
        <ProjectList />
      </PageContent>
    )
  }
}
const withConnect = connect(
  (state) => {
    const isLoaded = true
    if (isLoaded) {
      return {
        isLoaded
      }
    } else {
      return {
        isLoaded: false
      }
    }
  },
  (dispatch) => bindRoutineCreators({ }, dispatch),
)

const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(listProjects.trigger())
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('../../sagas/newProject'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(Projects)
