import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import ProjectLink from '../ProjectLink'
import { listProjects } from '../../actions'
import { selectProjectsList } from '../../reducers/projects'

import './project-list.scss'

class ProjectList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    this.props.listProjects.trigger()
  }

  createButton = () => {
    const { error, pristine, submitting } = this.props
    return (<div className="row">
    </div>)
  }

  render() {
    const { projects } = this.props
    if (projects.length === 0) {
      return (<div><p>No projects yet.</p>{this.createButton()}</div>)
    }
    return (
      <div className='project-list'>
        <ul className="projects" >
          <div>
            {projects.map((project, i) => {
              return (
                <li key={i}>
                  <ProjectLink name={project.name} project={project} key={i} active={false} />
                </li>
              )
            }
            )}
          </div>
        </ul>
        {this.createButton()}
      </div>
    )
  }
}

const withReducer = injectReducer(require('../../reducers/projects'))
const withSaga = injectSagas(require('../../sagas/projects'))

const withConnect = connect(
  (state) => {
    const projects = selectProjectsList(state)
    return {
      projects
    }
  },
  (dispatch) => bindRoutineCreators({listProjects }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ProjectList)
