import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import classnames from 'classnames'
import { NavLink, DateTime } from 'components'
import { ROUTES } from 'routes'
import { injectReducer, injectSagas } from 'utils/async'
import { changeProject } from '../actions'

class ProjectLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: props.project.name, active: true };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.changeProject.trigger({
      project: this.state.name
    })
    this.setState({ active: !this.state.active });
  }

  render() {
    const { active, project } = this.props
    const { id, name, createdAt, updatedAt } = project
    let classes = classnames('project-link', { active: this.state.active });
    return (
      active
        ? <strong> <DateTime value={updatedAt} /> {name}</strong>
        :
        <strong><button onClick={this.handleClick}>
          {name}
        </button> {updatedAt ? <DateTime value={updatedAt} />: ''}
        </strong>

    )
  }
}

const withSaga = injectSagas(require('../sagas/changeProject'))

const withConnect = connect(
  (state) => ({}),
  (dispatch) => bindRoutineCreators({ changeProject }, dispatch),
)

export default compose(
  withSaga,
  withConnect,
)(ProjectLink)
