import { listProjects } from '../actions'


export const KEY = 'projects'

const initialState = {
  isLoading: false,
  isLoaded: false,
  names: [],
  byName: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { projects } = payload || {}
  const { byName } = state

  switch (type) {
    case listProjects.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listProjects.SUCCESS:
      return {
        ...state,
        names: projects.map((project) => project.name),
        byName: projects.reduce((byName, project) => {
          byName[project.name] = project
          return byName
        }, byName),
        isLoaded: true,
      }

    case listProjects.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listProjects.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectProjects = (state) => state[KEY]
export const selectProjectsList = (state) => {
  const projects = selectProjects(state)
  return projects.names.map((name) => projects.byName[name])
}
