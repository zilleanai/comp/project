import { loadProjectData } from '../actions'


export const KEY = 'project'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { project } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadProjectData.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadProjectData.SUCCESS:
      if (!ids.includes(project.id)) {

        ids.push(project.id)
      }
      byId[project.id] = project
      return {
        ...state,
        ids,
        byId,
      }

    case loadProjectData.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectProjectData = (state) => state[KEY]
export const selectProjectDataById = (state, id) => selectProjectData(state).byId[id]
